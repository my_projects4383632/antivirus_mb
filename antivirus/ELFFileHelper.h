#ifndef ELFFILEHELPER_H_
#define ELFFILEHELPER_H_

#include <iostream>
#include <filesystem>
#include <fstream>
#include <vector> 

namespace fs = std::filesystem;


#if defined(__LP64__)
#define ElfW(type) Elf64_ ## type
#else
#define ElfW(type) Elf32_ ## type
#endif

#define CHUNK_SIZE 1024

class ELFFileHelper{
public:
    static bool isElfFile(const std::string& path);
    static bool isElfFIleInfected(std::string path, const std::vector<unsigned char> signature);
private:
    static bool lookForSignature(std::vector<unsigned char> buffer,
                                     std::vector<unsigned char> signature,
                                     int* match_count, std::vector<unsigned char>* currentPattern);
};

#endif