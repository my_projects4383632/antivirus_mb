#include "threadPool.h"

std::mutex infectedCaseMutex_;


// the func check if the file is ELF and if it soes, that check whether its infected or not. true for both of them
void checkFile(const std::string& path, const std::vector<unsigned char> signature, MainThreadHandler* handler){
    if (!ELFFileHelper::isElfFile(path)) {
        return;
    } 
    if(ELFFileHelper::isElfFIleInfected(path, signature)){
        infectedCaseMutex_.lock();       // open mutex for append infected file to the queue and print to the terminal
        
        handler->pushInfectedFile(path);        // append file to qwueue of infected ELF files
        std::cout << "\nFile " << path << " infeceted!" << std::endl;
        infectedCaseMutex_.unlock();
    }
}

// the func run on hanler's task queue and create for each a thread from the thread pool
int taskQueueMenagement(ThreadPool* pool, MainThreadHandler* handler){
    std::string path = "";
    const std::vector<unsigned char> signature = handler->getSignature();
    while(!handler->getIsScanningFinished()){    // Safety net for a situation that the queue is currently empty but the scanning for files not over yet
       while(!handler->isEmpty()){      // run until the queue is empty
            path = handler->getFirstFile();
            if(path != QUEUE_EMPTY){        // in case there was an error with the value retrieval
                pool->enqueue([path, signature, handler]{checkFile(path, signature, handler);});
            }
       }
    }

    pool->waitFinished();
    return 0;
}


ThreadPool::ThreadPool(unsigned int n)
    : busy()
    , processed()
    , stop()
{
    for (unsigned int i=0; i<n; ++i)
        workers.emplace_back(std::bind(&ThreadPool::thread_proc, this));
}

ThreadPool::~ThreadPool()
{
    // set stop-condition
    std::unique_lock<std::mutex> latch(queue_mutex);
    stop = true;
    cv_task.notify_all();
    latch.unlock();

    // all threads terminate, then we're done.
    for (auto& t : workers)
        t.join();
}

void ThreadPool::thread_proc()
{
    while (true)
    {
        std::unique_lock<std::mutex> latch(queue_mutex);
        cv_task.wait(latch, [this](){ return stop || !tasks.empty(); });
        if (!tasks.empty())
        {
            // got work. set busy.
            ++busy;

            // pull from queue
            auto fn = tasks.front();
            tasks.pop_front();

            // release lock. run async
            latch.unlock();

            // run function outside context
            fn();
            ++processed;

            latch.lock();
            --busy;
            cv_finished.notify_one();
        }
        else if (stop)
        {
            break;
        }
    }
}

// generic function push
template<class F>
void ThreadPool::enqueue(F&& f)
{
    std::unique_lock<std::mutex> lock(queue_mutex);
    tasks.emplace_back(std::forward<F>(f));
    cv_task.notify_one();
}

// waits until the queue is empty.
void ThreadPool::waitFinished()
{
    std::unique_lock<std::mutex> lock(queue_mutex);
    cv_finished.wait(lock, [this](){ return tasks.empty() && (busy == 0); });
}