#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <mutex>
#include <thread>
#include <functional>
#include <cassert>
#include <condition_variable>
#include <deque>
#include <atomic>

#include "mainThreadHandler.h"


class ThreadPool
{
public:
    // hardware_concurrency() - return num of cores, This is in order to exhaust the best abilities nad not make overload on the thread
    ThreadPool(unsigned int n);

    template<class F> void enqueue(F&& f);
    void waitFinished();
    ~ThreadPool();

    unsigned int getProcessed() const { return processed; }

private:
    std::vector< std::thread > workers;
    std::deque< std::function<void()> > tasks;
    std::mutex queue_mutex;
    std::condition_variable cv_task;
    std::condition_variable cv_finished;
    std::atomic_uint processed;
    unsigned int busy;
    bool stop;

    void thread_proc();
};

void checkFile(const std::string& path, const std::vector<char> signature, MainThreadHandler* handler);
int taskQueueMenagement(ThreadPool* pool, MainThreadHandler* handler);

#endif