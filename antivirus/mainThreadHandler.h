#ifndef MAINTHREADHANDLER_H_
#define MAINTHREADHANDLER_H_

#include <queue>

#include "ELFFileHelper.h"

#define DIR_INFECETED 1
#define DIR_CLEAN 0
#define VALID 0
#define INVALID 1
#define QUEUE_EMPTY "EMPTY"


class MainThreadHandler{
public:
// constructor distructor
    MainThreadHandler(std::vector<unsigned char> virusSignature){
        this->_virusSignature = virusSignature;
        this->_isScanningFinished = false; 
    }
    ~MainThreadHandler(){};

    void scanDir(fs::path path);

    inline void pushInfectedFile(std::string path){this->_infectedFiles.push(path);}
 
    void printMsg();

// getters&&setters
    inline const bool getIsScanningFinished(){return this->_isScanningFinished;}
    std::string getFirstFile();    
    inline const bool isEmpty(){return this->_filesQueue.empty();}
    inline const std::vector<unsigned char> getSignature(){return this->_virusSignature;}

    inline void setIsScanningFinished(bool isScanningFinished){this->_isScanningFinished = isScanningFinished;}

private:
// the access to the files qeueu should be limited
    inline void appendFile(std::string path){this->_filesQueue.push(path);}

    bool _isScanningFinished;           // gis value true only when the scannnig proccess is over
    std::queue<std::string> _filesQueue;        // queue of files to check
    std::queue<std::string> _infectedFiles;     // queue of onfeceted ELF files 
    std::vector<unsigned char> _virusSignature;     // signature of the virus
};

#endif
