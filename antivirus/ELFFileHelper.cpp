#include "ELFFileHelper.h"


// the func look for the signature inside the buffer
bool ELFFileHelper::lookForSignature(std::vector<unsigned char> buffer,
                                     std::vector<unsigned char> signature,
                                     int* match_count, std::vector<unsigned char>* currentPattern) {
    int index = 0;
    std::vector<unsigned char> temp;
    // if(std::search(buffer.begin(), buffer.end(),signature.begin(), signature.end()) != buffer.end()){
    //     return true;
    // }
    for (auto& it : buffer) {       // iterate over each element 
        if (it == signature[*match_count]) {            // in case the element equal to the signature in the current match_point
            currentPattern->push_back(it);
            (*match_count)++;       // move to the next element inside the signature
        } else {
            *match_count = 0;
            if(*match_count == 0){
                currentPattern->push_back(it);
                // check in case the signature hidden in pattern the looks like the signature
                // iterate over buffer that saved the chars until the Identification failed
                for (int t = 1; currentPattern->size() > t ; t++) {
                    if((*currentPattern)[t] == signature[*match_count]){
                        temp.push_back((*currentPattern)[t]);       
                        (*match_count)++;
                    }
                    else if(!temp.empty()){
                        temp.clear();
                        *match_count = 0;
                    }
                }
                currentPattern->clear();
                (*currentPattern) = temp;
                temp.clear();
            }
        }
        if (*match_count == signature.size()) {
            return true;
        }
        index++;
    } 
    return false;
}


// the func return true if the file is infected
bool ELFFileHelper::isElfFIleInfected(std::string path, const std::vector<unsigned char> signature){
    std::ifstream file(path, std::ios::binary);

    // copies all data into buffer
    std::vector<unsigned char> buffer(CHUNK_SIZE,0);

    int match_count = 0;        // this var for saving the sequence of the buffers
    // for example in case the signature existing exactly between the buffers scope

    // this method is for scaning efficiently in case the file is large
    // this method reading the file in multiple chucnks until end of file
    std::vector<unsigned char> currentPattern;
    while(!file.eof()) {        // until the cursor points on the end of the file
        file.read(reinterpret_cast<char*>(buffer.data()), buffer.size());       // read from the file num of bts as size of the buffer
        if(ELFFileHelper::lookForSignature(buffer, signature, &match_count, &currentPattern)){       // check if the signature inside the buffer
            file.close();  
            return true;        
        }
    }
    file.close();
    return false;
}


// the func return true if the file is ELF, otherwise false
bool ELFFileHelper::isElfFile(const std::string& path) {
    if(!fs::is_regular_file(path)){     // check if the file is a regular file and not block file||character file||fifo.....
        return false;
    }
    
    // Open the file in binary mode
    std::ifstream file(path, std::ios::binary);
    
    if (!file.is_open()) {
        // std::cerr << "Failed to open file: " << path << std::endl;
        return false;
    }

    // Read the first 4 bytes (magic number)
    std::vector<char> magicNumber(4);
    file.read(magicNumber.data(), 4);

    // Check if the magic number is '\x7FELF' - the signature of ELF file
    bool isElf = (magicNumber[0] == '\x7F') && (magicNumber[1] == 'E') && (magicNumber[2] == 'L') && (magicNumber[3] == 'F');

    // Close the file
    file.close();

    return isElf;
}
