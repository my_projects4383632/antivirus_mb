#include "mainThreadHandler.h"

// recursion func that iterate over all diractories in the given path and append the files to _filesQueue
void MainThreadHandler::scanDir(fs::path path){
    try {
        for (const auto& entry : fs::directory_iterator(path)) {
            if (entry.is_directory())       // directory
                this->scanDir(entry.path().string());
            else        // file
                this->appendFile(entry.path().string());     
        }
    } catch (const fs::filesystem_error& e) {
        e.what();
    }
}


// the func delete and return the first element from the queue of files to check
std::string MainThreadHandler::getFirstFile(){
    if(this->_filesQueue.empty()){
        return QUEUE_EMPTY;
    }
    std::string f = this->_filesQueue.front();
    this->_filesQueue.pop(); 
    return f;
}    


// print Summary notice
void MainThreadHandler::printMsg(){
    if(this->_infectedFiles.empty()){
        std::cout << "\nAll files are clear!\n" << std::endl;
    }
    else{
        std::cout  << "\nThere are " << this->_infectedFiles.size() << " infected files!\n" << std::endl;
    }
}