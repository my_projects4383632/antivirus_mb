#include "threadPool.h"

#define DIR_INDEX 1
#define SIGNATURE_PATH_INDEX 2
#define FILE_ERR 1
#define PARAMETERS 3

// take the virus signature from the file that store it
std::vector<unsigned char> getViruSignature(std::string path){
    // get signature of the virus as a string
    std::ifstream input(path, std::ios::binary );
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});
    if(!buffer.empty() && buffer.back() == '\n'){     // erasing the \n from the end of the vecotr
        buffer.pop_back();      
    }
    return buffer;
}

int main(int argc, char* argv[]) {
    
    if(argc != PARAMETERS){
        std::cerr << "wrong parameters!" << std::endl;
        return 1;
    }
    // get parameters
    std::vector<unsigned char> buffer = getViruSignature(argv[SIGNATURE_PATH_INDEX]);       
    fs::path path = argv[DIR_INDEX];
    
    MainThreadHandler* handler = new MainThreadHandler(buffer);       // create an object

    // set up the thread pool
    ThreadPool pool(std::thread::hardware_concurrency() * 4); 
    std::thread thread(taskQueueMenagement, &pool, handler);   // start second thread that menage the thread pool
    
    // set up the sacnner 
    std::cout << "Scanning started..." << std::endl;
    handler->scanDir(path);   

    // scan finished
    handler->setIsScanningFinished(true);

    thread.join();      // waits until the thread finish
    handler->printMsg();
    delete handler;
    
    return 0;
}